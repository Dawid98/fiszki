# -*- coding: utf-8 -*-
from faker import Faker
import random
f = Faker('pl_PL')

file_object  = open("tags-directiories.sql", "w")

list = ["Stajenny",
        "Technik handlowiec",
        "Technik żywienia i gospodarstwa domowego",
        "Brakarz",
        "Projektant gier komputerowych",
        "Ergonomista",
        "Audytor efektywności energetycznej",
        "Lalkarz",
        "Pokojówka",
        "Bibliotekarz dyplomowany",
        "Archiwista",
        "Poganiacz",
        "Rzecznik prasowy",
        "Archiwista",
        "Portier",
        "Rzeczoznawca budowlany",
        "Technik technologii drewna",
        "Ratownik przedmedyczny",
        "Reżyser",
        "Konsjerż"]

n = 1
for i in range(1,43):
    s = "INSERT INTO fiszki.directory_tags value ({id},'{tags_name}');\n"
    file_object.write(s.format(id=n,tags_name=random.choice(list)))
    n+=1
file_object.close()
