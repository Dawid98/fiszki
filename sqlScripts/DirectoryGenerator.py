from faker import Faker
import random
f = Faker('pl_PL')
list = ["Fizyka",
        "Matematyko",
        "OOP",
        "Malarstwo",
        "Literatura",
        "Książki",
        "Filmy",
        "WOS",
        "Historia",
        "Ekonomia",
        "Plastyka",
        "Kelnerstwo",
        "Scenografia",
        "Wiedza ogulna",
        "Programowanie",
        "AOP"]
listOfUsers = []
f = Faker('pl_PL')

file_object  = open("directories.sql", "w")
for i in range(10,31):
    for n in range(1,3):
        s = "INSERT INTO fiszki.directory VALUE ({id},'{word}',{quiz},'{category_id}',{user});\n"
        file_object.write(s.format(id=n*(i-9),word=f.word(),quiz=random.choice([0,1]),category_id=random.choice(list),user=i))
file_object.close()
