package com.BranchCompany.Fiszki.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="user")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private int id;

    @Column(name = "email",unique = true)
    private String email;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "userss", cascade = CascadeType.ALL)
    private List<Directory> directories = new LinkedList<>();

    public User(){}
    public User( String email, String password) {
        this.email = email;
        this.password = password;
    }

}
