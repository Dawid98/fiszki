package com.BranchCompany.Fiszki.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name="directory")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Directory implements Cloneable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column( name = "name" )
    private String name;

    @Column(name = "quiz")
    private boolean quiz;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="category_id")
    private Category categoriess;

    @Column(name = "description")
    private String description;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL )
    private List<Tag> tags = new LinkedList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "directories", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FlashCard> flashCards = new LinkedList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_id")
    private User userss;

    public Directory(String name, boolean quiz) {
        this.name = name;
        this.quiz = quiz;
    }

    public Directory() {}

    @Override
    public Directory clone() throws CloneNotSupportedException {
        Directory newDirectory = (Directory) super.clone();
        newDirectory.id = null;
        newDirectory.setUserss(null);
        newDirectory.setTags(new LinkedList<>());
        newDirectory.setCategoriess(newDirectory.getCategoriess().clone());
        List<FlashCard> copiedFLashCards = newDirectory.getFlashCards().stream().map( flashCard -> {
            try {
                return flashCard.clone();
            } catch (CloneNotSupportedException e){
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());
        newDirectory.setFlashCards(copiedFLashCards);
        return newDirectory;
    }
}
