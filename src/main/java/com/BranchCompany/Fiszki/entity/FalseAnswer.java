package com.BranchCompany.Fiszki.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="falseAnswer")
@Data
public class FalseAnswer implements Cloneable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column( name = "wrongAnswer")
    private String wrongAnswer;

    @ManyToOne
    @JoinColumn(name = "flash_Card_id")
    private FlashCard flashCard;

    @Override
    protected FalseAnswer clone() throws CloneNotSupportedException {
        FalseAnswer falseAnswer = (FalseAnswer) super.clone();
        falseAnswer.id = null;
        return falseAnswer ;
    }
}
