package com.BranchCompany.Fiszki.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="tag")
@Data
public class Tag {


    @Id
    @Column(name="name")
    private String name;

    @ManyToMany(mappedBy = "tags" )
    private List<Directory> directories;

    public Tag(){}
    public Tag(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "Test : " + " Name="+name + "]";
    }

}
