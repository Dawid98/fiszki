package com.BranchCompany.Fiszki.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name="flashCard")
@Data
public class FlashCard implements Cloneable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name ="id")
    private Integer id;

    @Column(name = "question")
    private String question;

    @Column( name = "trueAnswer")
    private String trueAnswer;

    @ManyToOne
    @JoinColumn(name="directory_id")
    private Directory directories;

    @OneToMany(mappedBy = "flashCard", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FalseAnswer> falseAnswers = new LinkedList<>();

    @Override
    public FlashCard clone() throws CloneNotSupportedException {
         FlashCard copiedFlashCard = (FlashCard) super.clone();
         copiedFlashCard.id = null;
         copiedFlashCard.setDirectories(null);
         copiedFlashCard.setFalseAnswers(new LinkedList<>());
        return copiedFlashCard;
    }
}
