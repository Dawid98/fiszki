package com.BranchCompany.Fiszki.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import sun.security.x509.CertAttrSet;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="category")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Category implements Cloneable {

    @Id
    @Column(name = "name")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "categoriess", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Directory> directories = new LinkedList<>();

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    @Override
    protected Category clone() throws CloneNotSupportedException {
        Category category = (Category) super.clone();
        category.directories = null;
        return category;
    }
}
