package com.BranchCompany.Fiszki.dao;

import com.BranchCompany.Fiszki.entity.FalseAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface FalseAnswerRepository extends JpaRepository<FalseAnswer,Integer> {
}
