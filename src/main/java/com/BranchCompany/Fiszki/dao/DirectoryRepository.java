package com.BranchCompany.Fiszki.dao;

import com.BranchCompany.Fiszki.entity.Category;
import com.BranchCompany.Fiszki.entity.Directory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;


import java.util.List;

@CrossOrigin
public interface DirectoryRepository extends JpaRepository<Directory, Integer> {

    @Query("SELECT d FROM Directory d WHERE (d.categoriess = :category AND d.description LIKE %:keyword%) OR (d.categoriess = :category AND d.name LIKE %:keyword%) ")
    Page<Directory> searchAllByKeyWordAndCategory(@Param("keyword") String keyword, @Param("category") Category categoriess, Pageable pageable);

    @Query("SELECT d FROM Directory d WHERE d.description LIKE %:keyword% OR  d.name LIKE %:keyword% ")
    Page<Directory> searchAllByKeyWord(@Param("keyword") String keyword, Pageable pageable);

}
