package com.BranchCompany.Fiszki.dao;

import com.BranchCompany.Fiszki.entity.FlashCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface FlashCardRepository extends JpaRepository<FlashCard,Integer> {
}
