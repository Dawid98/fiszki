package com.BranchCompany.Fiszki.dao;

import com.BranchCompany.Fiszki.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface TagRepository extends JpaRepository<Tag,String> {
}
