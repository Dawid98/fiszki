package com.BranchCompany.Fiszki.dao;

import com.BranchCompany.Fiszki.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin
@RepositoryRestResource(path = "users",
        collectionResourceRel = "users")
public interface UserRepository extends JpaRepository<User, Integer> {

    @RestResource(exported = false)
    User findByEmail(String email);

    @RestResource(exported = false)
    List<User> findAll();

}
