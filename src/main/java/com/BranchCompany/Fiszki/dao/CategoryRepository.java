package com.BranchCompany.Fiszki.dao;

import com.BranchCompany.Fiszki.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface CategoryRepository extends JpaRepository<Category,String> {
}
