package com.BranchCompany.Fiszki.model;

import com.BranchCompany.Fiszki.entity.Directory;
import lombok.Data;

import java.util.List;

@Data
public class SearchResultModel {
    SearchModel searchModel;
    List<Directory> directoryList;
    Long amountOfPages;

    public SearchResultModel(SearchModel searchModel, List<Directory> directoryList, Long amountOfPages) {
        this.searchModel = searchModel;
        this.directoryList = directoryList;
        this.amountOfPages = amountOfPages;
    }

    public SearchResultModel() {
    }
}
