package com.BranchCompany.Fiszki.model;

import com.BranchCompany.Fiszki.entity.Category;
import lombok.Data;

@Data
public class SearchModel {
    String keyword;
    String categoryId;
    Integer pageNumber;
    Integer sizeOfPage;

    public SearchModel(String keyword, String categoryId, Integer pageNumber, Integer sizeOfPage) {
        this.keyword = keyword;
        this.categoryId = categoryId;
        this.pageNumber = pageNumber;
        this.sizeOfPage = sizeOfPage;
    }

    public SearchModel() {
    }

    static class SearchModelBuilder {
        private SearchModel searchModel = new SearchModel("","",0,20);

        public SearchModelBuilder buildKeyWord(String keyWord){
            searchModel.setKeyword(keyWord);
            return this;
        }

        public SearchModelBuilder buildCategoryID(String categoryId){
            searchModel.setCategoryId(categoryId);
            return this;
        }

        public SearchModelBuilder buildCategoryID(Category category){
            searchModel.setCategoryId(category.getName());
            return this;
        }

        public SearchModelBuilder buildPageNumber(Integer pageNumber){
            searchModel.setPageNumber(pageNumber);
            return this;
        }

        public  SearchModelBuilder buildSizeOfPage(Integer sizeOfPage){
            searchModel.setSizeOfPage(sizeOfPage);
            return this;
        }

        public SearchModel getResult(){
            return searchModel;
        }

    }

}
