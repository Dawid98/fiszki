package com.BranchCompany.Fiszki.config;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.Entity;
import java.util.Set;

@Configuration
@EnableWebMvc
public class RepositoryRestConfig extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(final RepositoryRestConfiguration config) {

        final ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(
                false);
        provider.addIncludeFilter(new AnnotationTypeFilter(Entity.class));

        final Set<BeanDefinition> beans = provider.findCandidateComponents("com.BranchCompany.Fiszki");

        for (final BeanDefinition bean : beans) {
            try {
                config.exposeIdsFor(Class.forName(bean.getBeanClassName()));
            } catch (final ClassNotFoundException e) {
                throw new IllegalStateException("Failed to expose `id` field due to", e);
            }
        }
    }


}
