package com.BranchCompany.Fiszki.service;

import com.BranchCompany.Fiszki.dao.DirectoryRepository;
import com.BranchCompany.Fiszki.dao.FalseAnswerRepository;
import com.BranchCompany.Fiszki.dao.FlashCardRepository;
import com.BranchCompany.Fiszki.dao.UserRepository;
import com.BranchCompany.Fiszki.entity.Directory;
import com.BranchCompany.Fiszki.entity.FlashCard;
import com.BranchCompany.Fiszki.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class KakashiService {

    @Autowired
    private DirectoryRepository directoryRepository;

    @Autowired
    private FlashCardRepository flashCardRepository;


    @Autowired
    private UserRepository userRepository;


    public Directory copyDirectory(final Integer idOfDirectory, final Integer idOfUser) throws CloneNotSupportedException {
        Directory directory = directoryRepository.getOne(idOfDirectory);
        User user = userRepository.getOne(idOfUser);
        Directory copiedDirectory = directory.clone();
        copiedDirectory.setUserss(user);
        List<FlashCard> flashCardList = copiedDirectory.getFlashCards();
        copiedDirectory.setFlashCards(new LinkedList<>());
        directoryRepository.save(copiedDirectory);
        flashCardList.forEach(flashCard -> flashCard.setDirectories(copiedDirectory));
        flashCardRepository.saveAll(flashCardList);
        copiedDirectory.setUserss(null);
        copiedDirectory.setFlashCards(null);
        return copiedDirectory;
    }
}
