package com.BranchCompany.Fiszki.service;

import com.BranchCompany.Fiszki.dao.CategoryRepository;
import com.BranchCompany.Fiszki.dao.DirectoryRepository;
import com.BranchCompany.Fiszki.entity.Category;
import com.BranchCompany.Fiszki.entity.Directory;
import com.BranchCompany.Fiszki.model.SearchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchService {

    @Autowired
    private DirectoryRepository directoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;


    public Page<Directory> searchForDirectories(SearchModel searchModel) {
        Pageable pageable = PageRequest.of(searchModel.getPageNumber(), searchModel.getSizeOfPage());
        String keyWord = searchModel.getKeyword();
        Page<Directory> directories;
        if (searchModel.getCategoryId().equals(""))
            directories = directoryRepository.searchAllByKeyWord(keyWord, pageable);
        else {
            Category category = categoryRepository.getOne(searchModel.getCategoryId());
            directories = directoryRepository.searchAllByKeyWordAndCategory(keyWord, category, pageable);
        }
        return directories;
    }


}
