package com.BranchCompany.Fiszki.endpoint;

import com.BranchCompany.Fiszki.entity.Directory;
import com.BranchCompany.Fiszki.model.CopyModel;
import com.BranchCompany.Fiszki.service.KakashiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CopyController {

    @Autowired
    KakashiService kakashiService;

    @PostMapping("/copy-dir")
    public ResponseEntity responseEntity(@RequestBody CopyModel copyModel) throws CloneNotSupportedException{
        Directory copiedDir = kakashiService.copyDirectory(copyModel.idOfDir,copyModel.idOfUser);
        return ResponseEntity.ok(copiedDir);
    }
}
