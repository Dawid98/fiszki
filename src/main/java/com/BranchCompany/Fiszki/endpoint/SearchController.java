package com.BranchCompany.Fiszki.endpoint;

import com.BranchCompany.Fiszki.entity.Directory;
import com.BranchCompany.Fiszki.model.SearchModel;
import com.BranchCompany.Fiszki.model.SearchResultModel;
import com.BranchCompany.Fiszki.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class SearchController {

    @Autowired
    private SearchService searchService;

    @PostMapping("directories/search")
    public ResponseEntity<SearchResultModel> searchForDirectories(@RequestBody SearchModel searchModel) {
        final Page<Directory> directories = searchService.searchForDirectories(searchModel);
        long amountOfPages = directories.getTotalPages();
        SearchResultModel searchResultModel = new SearchResultModel(searchModel,directories.getContent(), amountOfPages);
        return  ResponseEntity.ok(searchResultModel);
    }

}
